<?php

namespace OmekaTheme\Helper;

use Laminas\View\Helper\AbstractHelper;

class StringUrl extends AbstractHelper
{
    /**
     * Get the full url from a complete or partial url setting.
     *
     * @param string $uri Partial or full url.
     * @return string
     */
    public function __invoke($uri)
    {
        if (empty($uri)
            || strpos($uri, '/') === 0
            || strpos($uri, 'https://') === 0
            || strpos($uri, 'https://') === 0
            || $uri === '#'
        ) {
            return $uri;
        }

        $baseUrl = $this->currentSite()->url() . '/';

        // Check for string like page/about or item/browse.
        return strpos($uri, '/')
            ? $baseUrl . $uri
            : $baseUrl . 'page/' . $uri;
    }

    /**
     * Get the current site.
     */
    protected function currentSite(): ?\Omeka\Api\Representation\SiteRepresentation
    {
        return $this->view->site ?? $this->view
            ->getHelperPluginManager()
            ->get('Laminas\View\Helper\ViewModel')
            ->getRoot()
            ->getVariable('site');
    }
}
