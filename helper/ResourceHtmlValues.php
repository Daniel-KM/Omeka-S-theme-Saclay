<?php
namespace OmekaTheme\Helper;

use Laminas\View\Helper\AbstractHelper;
use Omeka\Api\Representation\AbstractResourceEntityRepresentation;

class ResourceHtmlValues extends AbstractHelper
{
    /**
     * Returns the list of property values as html for a resource.
     *
     * @param AbstractResourceEntityRepresentation $resource
     * @param string $property
     * @param array $options Options to get resource values.
     * @return array
     */
    public function __invoke(
        AbstractResourceEntityRepresentation $resource,
        $property,
        array $options = ['all' => true]
    ) {
        // Value::asHtml() is used because it is the simplest way to get values.
        if (empty($options['all'])) {
            $value = $resource->value($property, $options);
            return $value ? $value->asHtml() : $value;
        }
        return array_map(function ($v) {
            return $v->asHtml();
        }, $resource->value($property, $options));
    }
}
