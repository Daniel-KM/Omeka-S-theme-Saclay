Saclay (theme for Omeka S)
==========================

> __New versions of this theme and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the old repository.__

[Saclay] est un theme pour [Omeka S] conçu pour le [service of communication] de
l’[Université Paris-Saclay].

Il nécessite le [module Saclay].

Tout le contenu du site est fourni, y compris les images.

**WARNING**: The logos cannot be reused without express permissions.


Installation
------------

This theme requires the [module Saclay], that provides the search block for the
home page.

This theme works better with these associated modules:
- [Advanced Search Plus]
- [Block Plus]
- [Universal Viewer], [Iiif Server], and [Image Server]
- [Meta Tags]
- [Next]
- [Reference]

See general end user documentation for [installing a module].

* From the zip

Download the last release [Saclay.zip] from the list of releases, and uncompress
it in the `themes` directory.

* From the source and for development

If the module was installed from the source, rename the name of the folder of
the theme folder as you want, preferably without spaces and special characters.
Then go to the root of the module, and run:

```sh
npm install
gulp
```


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [theme issues] page on GitLab.


License
-------

This theme, **except the logos**, is published under the [CeCILL v2.1] license,
compatible with [GNU/GPL] and approved by [FSF] and [OSI].

In consideration of access to the source code and the rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors only have limited liability.

In this respect, the risks associated with loading, using, modifying and/or
developing or reproducing the software by the user are brought to the user’s
attention, given its Free Software status, which may make it complicated to use,
with the result that its use is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore encouraged
to load and test the suitability of the software as regards their requirements
in conditions enabling the security of their systems and/or data to be ensured
and, more generally, to use and operate it in the same conditions of security.
This Agreement may be freely reproduced and published, provided it is not
altered, and that no provisions are either added or removed herefrom.


Copyright
---------

* Copyright Daniel Berthereau, 2019-2021 (integration for [Sempiternelia], see [Daniel-KM] on GitLab)

The theme was initially based on the theme [PSL].


[Saclay]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Saclay
[module Saclay]: https://gitlab.com/Daniel-KM/Omeka-S-module-Saclay
[Omeka S]: https://omeka.org/s
[service of communication]: https://www.universite-paris-saclay.fr/
[Université Paris-Saclay]: https://www.universite-paris-saclay.fr/
[thème Saclay]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Saclay
[installing a module]: http://dev.omeka.org/docs/s/user-manual/modules/#installing-modules
[Advanced Search Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedSearchPlus
[Block Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-BlockPlus
[Generic]: https://gitlab.com/Daniel-KM/Omeka-S-module-Generic
[ApiInfo]: https://gitlab.com/Daniel-KM/Omeka-S-module-ApiInfo
[Meta Tags]: https://gitlab.com/Daniel-KM/Omeka-S-module-MetaTags
[Reference]: https://gitlab.com/Daniel-KM/Omeka-S-module-Reference
[Next]: https://gitlab.com/Daniel-KM/Omeka-S-module-Next
[Universal Viewer]: https://gitlab.com/Daniel-KM/Omeka-S-module-UniversalViewer
[Iiif Server]: https://gitlab.com/Daniel-KM/Omeka-S-module-IiifServer
[Image Server]: https://gitlab.com/Daniel-KM/Omeka-S-module-ImageServer
[theme issues]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Saclay/-/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[GitLab]: https://gitlab.com/Daniel-KM
[PSL]: https://github.com/Daniel-KM/Omeka-S-theme-psl
[Sempiternelia]: https://sempiternelia.net
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
