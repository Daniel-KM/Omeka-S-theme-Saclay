<?php
/**
 * Ce fichier est à copier dans le thème.
 */

// Utiliser null pour faire une correspondance automatique avec les noms des icones.
// return null;

/**
 * Sinon correspondance entre les catégories (classes ou modèles de ressources) et les icones.
 */

return [
    // 'dcterms:Image' => 'image',
    // 'base resource' => 'default',
    'archive textuelle' => 'reportages',
    'archives textuelles' => 'reportages',
    'audio' => 'videos',
    'base resource' => '',
    'collectivite' => 'etablissements',
    'collectivité' => 'etablissements',
    'etablissement' => 'etablissements',
    'etablissements' => 'etablissements',
    'image' => 'photos',
    'images' => 'photos',
    'infographie' => 'infographies',
    'infographies' => 'infographies',
    'objet' => '',
    'objets' => '',
    'person' => '',
    'personne' => '',
    'personnes' => '',
    'photo' => 'photos',
    'photos' => 'photos',
    'plaquette' => 'plaquettes',
    'plaquettes' => 'plaquettes',
    'publication' => 'plaquettes',
    'publications' => 'plaquettes',
    'publication en série' => 'plaquettes',
    'publication en serie' => 'plaquettes',
    'publications en série' => 'plaquettes',
    'publications en serie' => 'plaquettes',
    'revue' => 'plaquettes',
    'revues' => 'plaquettes',
    'testmodel' => '',
    'sound' => 'videos',
    'vidéo' => 'videos',
    'video' => 'videos',
];
