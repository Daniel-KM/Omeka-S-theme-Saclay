document.addEventListener('DOMContentLoaded', function(event) {

/* Form fields to upload files. */
if (typeof bsCustomFileInput !== 'undefined') {
    bsCustomFileInput.init();
}

(function () {

    /* Item show */

    // When openseadragon is used with LightSlider, its size should be reset.
    $(window).bind('load', function () {
        var osd = $('.media-list .openseadragon');
        if (osd) {
            osd.css('width', osd.closest('.item-image').parent().width());
            var maxHeight = $(window).height();
            var currentHeight = osd.height();
            // Manage the case where the tiled image is not the first image:
            // in that case, take the first height of the first li.
            if (!osd.closest('li').is(':first-child')) {
                osd.css('height', osd.closest('ul').find('li:first').height());
            } else if (currentHeight > maxHeight) {
                osd.css('height', maxHeight);
            } else {
                osd.css('height', osd.closest('.item-image').parent().height());
            }
        }
    });

    // Add a layer on viewers to avoid to zoom when scrolling. Require a click.
    $('#media-uv, #mapping-map').wrap('<div class="click-to-zoom" title="Click to use viewer"><div class="pointer-events-none"></div></div>');
    $('.click-to-zoom').on('click', function() {
        $(this).find('.pointer-events-none > div').unwrap().unwrap();
    });

    // Lightslider (item/show).
    if ($.isFunction($.fn.lightSlider)) {

        $('#gallery').lightSlider({
            mode: 'fade',
            autoWidth: true,
            adaptiveHeight: false,
            gallery: true,
            item: 1,
            loop: true,
            thumbItem: 15,
            slideMargin: 0,
            enableDrag: false,
            currentPagerPosition: 'middle',
            pager: $('#gallery .resource.media').length > 1,
            onSliderLoad: function(el) {
                el.lightGallery({
                    selector: '#gallery .media',
                    download: false,
                    share: true,
                    zoom: true,
                    youtubePlayerParams: {
                        modestbranding: 1,
                        showinfo: 0,
                        rel: 0,
                        controls: 1
                    },
                    vimeoPlayerParams: {
                        byline : 0,
                        portrait : 0,
                        color : 'A90707'
                    },
                });
            }
        });
    }

})();

});
