'use strict';

const del = require('del');
const gulp = require('gulp');
const gulpif = require('gulp-if');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');

const bundle = [
    // TODO Remove useless files (from bootstrap…).
    {
        'source': 'node_modules/bootstrap/dist/**',
        'dest': 'asset/vendor/bootstrap',
    },
    {
        'source': 'node_modules/bs-custom-file-input/dist/bs-custom-file-input.min.js',
        'dest': 'asset/vendor/bs-custom-file-input',

    },
    // TODO Minify css and js.
    /*
    {
        'source': 'node_modules/daterangepicker/daterangepicker.css',
        'dest': 'asset/vendor/daterangepicker',
    },
    {
        'source': 'node_modules/daterangepicker/daterangepicker.js',
        'dest': 'asset/vendor/daterangepicker',
    },
    */
    {
        'source': 'node_modules/font-awesome/css/**',
        'dest': 'asset/vendor/font-awesome/css',
    },
    {
        'source': 'node_modules/font-awesome/fonts/**',
        'dest': 'asset/vendor/font-awesome/fonts',
    },
    {
        'source': 'node_modules/jquery/dist/jquery.min.js',
        'dest': 'asset/vendor/jquery',
    },
    {
        'source': 'node_modules/jquery-migrate/dist/jquery-migrate.min.js',
        'dest': 'asset/vendor/jquery',
    },
    {
        'source': 'node_modules/jquery-mousewheel/jquery.mousewheel.js',
        'dest': 'asset/vendor/jquery-mousewheel',
    },
    // Useless: included in light-gallery all.
    /*
    {
        'source': 'node_modules/lg-video/dist/lg-video.min.js',
        'dest': 'asset/vendor/lg-video',
    },
    {
        'source': 'node_modules/lg-zoom/dist/lg-zoom.min.js',
        'dest': 'asset/vendor/lg-zoom',
    },
    */
    {
        'source': 'node_modules/lightgallery/dist/**',
        'dest': 'asset/vendor/lightgallery',
    },
    {
        'source': 'node_modules/lightslider/dist/**',
        'dest': 'asset/vendor/lightslider',
    },
    // TODO Find the right library.
    /*
    {
        'source': 'node_modules/mobile-nav/dist/**',
        'dest': 'asset/vendor/mobile-nav',
    },
    */
];

gulp.task('clean', function(done) {
    bundle.forEach(function (module) {
        return del.sync(module.dest);
    });
    done();
});

gulp.task('sync', function (done) {
    bundle.forEach(function (module) {
        gulp.src(module.source)
            .pipe(gulpif(module.rename && module.rename.length, rename(module.rename)))
            .pipe(gulpif(module.suffix, rename({suffix: module.suffix})))
            .pipe(gulpif(module.uglify, uglify()))
            .pipe(gulp.dest(module.dest));
    });
    done();
});

gulp.task('default', gulp.series('clean', 'sync'));

gulp.task('install', gulp.task('default'));

gulp.task('css', function () {
    var sass = require('gulp-sass');
    var postcss = require('gulp-postcss');
    var autoprefixer = require('autoprefixer');

    return gulp.src('./asset/sass/*.scss')
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: ['node_modules/susy/sass']
        }).on('error', sass.logError))
        .pipe(postcss([
            autoprefixer()
        ]))
        .pipe(gulp.dest('./asset/css'));
});

gulp.task('css:watch', function () {
    gulp.watch('./asset/sass/*.scss', gulp.parallel('css'));
});
